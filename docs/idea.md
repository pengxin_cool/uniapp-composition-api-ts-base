### 声明页面/组件的案例

```typescript
<template>
  <view class="example"></view>
</template>
<script lang="ts">
  import { defineComponent } from '@vue/composition-api';

  export default defineComponent({
    setup() {}
  });
</script>

<style lang="scss" scoped>
  .example {
  }
</style>
```
