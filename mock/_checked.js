
    /**
     * file to record checked status when exit mock server
     * can be deleted if not needed
     * should ignored from git
     */
    module.exports = {
      mockChecked: {"test.com/getExample":0,"test.com/uploadFile":0},
      setChecked: null
    }
  