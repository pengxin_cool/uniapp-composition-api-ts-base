export function wait(cb: Function, time = 150) {
  setTimeout(() => {
    cb();
  }, time);
}

export function basePromise(cb: Function): Promise<unknown> {
  return new Promise((resolve, reject) => {
    cb(resolve, reject);
  });
}
