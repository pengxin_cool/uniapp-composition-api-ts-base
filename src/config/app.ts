/**
 * 整个项目相关配置
 */
export const APP_CONFIG = {
  title: 'O₂广场',
  theme: 'black' // 夜间主题
};
