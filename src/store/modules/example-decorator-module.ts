/**
 * 装饰器版本
 */
import { getModule, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import store from '@/store';

/**
 * 示例模块的store
 */
@Module({ dynamic: true, store, name: 'example', namespaced: true })
class ExampleStore extends VuexModule {
  public name = 'test';

  @Mutation
  public changeName(payload: string) {
    this.name = payload;
  }
}

/**
 * 示例模块的store模块
 */
const exampleModule = getModule(ExampleStore);
export default exampleModule;
