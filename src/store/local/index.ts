/**
 * 持久化插件
 */
import createPersistedState from 'vuex-persistedstate';
// import { localStorage } from 'mp-storage'; // 直接进行设置
export const local = createPersistedState({
  // 可以有多个持久化实例
  key: 'app_config_data', // 状态保存到本地的 key
  //   paths: ['token', 'username'], // 要持久化的状态，在state里面取，如果有嵌套，可以  a.b.c
  storage: {
    // 存储方式定义
    getItem: (key) => uni.getStorageSync(key), // 获取
    setItem: (key, value) => uni.setStorageSync(key, value), // 存储
    removeItem: (key) => uni.removeStorageSync(key) // 删除
  }
});
