export interface DefaultConfigType {
  [key: string]: number;
  time: number;
}

/**
 * 弹窗默认图片
 */
export enum icons {
  successImg = '/static/message/success.png',
  errorImg = '/static/message/error.png',
  warningImg = '/static/message/warning.png'
}

export const DEFAULT_CONFIG: DefaultConfigType = {
  time: 1000
};

export const setMessageConfig = (config?: DefaultConfigType) => {
  if (!config) return;
  Object.keys(config).forEach((key) => {
    if (DEFAULT_CONFIG[key]) {
      DEFAULT_CONFIG[key] = config[key];
    }
  });
};
