import { basePromise, wait } from '@/utils/common/index';
import { setMessageConfig, icons, DEFAULT_CONFIG } from './config';

export { setMessageConfig };

const Message = {
  /**
   * 成功弹窗
   * @param {*} title 弹窗内容
   * @param {*} config 弹窗配置
   * @returns 返回promise化的弹窗
   */
  // eslint-disable-next-line no-undef
  success(title: string, config?: UniApp.ShowToastOptions) {
    return basePromise((resolve: (arg0: any) => void) => {
      uni.showToast({
        ...config,
        title,
        image: icons.successImg,
        duration: config?.duration || DEFAULT_CONFIG.time,
        // mask: (config && config.mask) || true,
        complete(res) {
          wait(() => {
            resolve(res);
          }, config?.duration || DEFAULT_CONFIG.time);
        }
      });
    });
  },
  /**
   * 错误弹窗
   * @param {*} title 弹窗内容
   * @param {*} config 弹窗配置
   * @returns 返回promise化的弹窗
   */
  // eslint-disable-next-line no-undef
  error(title: any, config?: UniApp.ShowToastOptions) {
    return basePromise((resolve: (arg0: any) => void) => {
      uni.showToast({
        ...config,
        title,
        image: icons.errorImg,
        duration: config?.duration || DEFAULT_CONFIG.time,
        mask: config?.mask || true,
        complete(res) {
          wait(() => {
            resolve(res);
          }, config?.duration || DEFAULT_CONFIG.time);
        }
      });
    });
  },
  /**
   * 警告弹窗
   * @param {*} title 弹窗内容
   * @param {*} config 弹窗配置
   * @returns 返回promise化的弹窗
   */
  // eslint-disable-next-line no-undef
  warning(title: any, config?: UniApp.ShowToastOptions) {
    return basePromise((resolve: (arg0: any) => void) => {
      uni.showToast({
        ...config,
        title,
        image: icons.warningImg,
        duration: config?.duration || DEFAULT_CONFIG.time,
        mask: config?.mask || true,
        complete(res) {
          wait(() => {
            resolve(res);
          }, config?.duration || DEFAULT_CONFIG.time);
        }
      });
    });
  },
  /**
   * 普通弹窗
   * @param {*} title 弹窗内容
   * @param {*} config 弹窗配置
   * @returns 返回promise化的弹窗
   */
  // eslint-disable-next-line no-undef
  other(title: any, config?: UniApp.ShowToastOptions) {
    return basePromise((resolve: (arg0: any) => void) => {
      uni.showToast({
        ...config,
        title,
        icon: config?.icon || 'none',
        duration: config?.duration || DEFAULT_CONFIG.time,
        mask: config?.mask || true,
        complete(res) {
          wait(() => {
            resolve(res);
          }, config?.duration || DEFAULT_CONFIG.time);
        }
      });
    });
  }
};

export default Message;
