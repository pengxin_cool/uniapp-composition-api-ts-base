import { AxiosRequestConfig } from 'axios';

/**
 * 设置请求拦截器
 * @param config AxiosRequestConfig axios的请求配置
 * @returns 修改后的axios的请求配置
 */
export const setRequestConfig = (config: AxiosRequestConfig): AxiosRequestConfig => {
  // const token = store.state.token;
  // token && (config.headers['token'] = token);
  return config;
};

/**
 * 设置请求错误拦截器
 * @param error 报错内容
 * @returns 错误装饰的promise
 */
export const setRequestError = (error: any) => {
  return Promise.reject(error);
};
