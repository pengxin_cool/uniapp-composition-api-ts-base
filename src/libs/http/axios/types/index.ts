/**
 * 服务端响应数据结构
 */
export interface ResponseDataType<T = any> {
  code?: number;
  data?: T;
  msg?: string;
  message?: string;
}
