/* eslint-disable no-undef */
import { AxiosRequestConfig, AxiosResponse } from 'axios';

const settle: Function = require('axios/lib/core/settle');

export interface FitUniappResponse {
  data: UniApp.RequestSuccessCallbackResult['data'];
  status: UniApp.RequestSuccessCallbackResult['statusCode'];
  errMsg: UniApp.GeneralCallbackResult['errMsg'];
  header: UniApp.RequestSuccessCallbackResult['header'];
  config: AxiosRequestConfig;
}

/**
 * Resolve or reject a Promise based on response status.
 *
 * @param {Function} resolve A function that resolves the promise.
 * @param {Function} reject A function that rejects the promise.
 * @param {object} response The response.
 */
function settleTools(resolve: Function, reject: Function, response: FitUniappResponse): AxiosResponse {
  return settle(resolve, reject, response);
}
export default settleTools;
