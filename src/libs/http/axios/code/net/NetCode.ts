/**
 * 网关错误状态码
 */
const RESULT_CODE: Record<RESULT_CODE_KEY, string | undefined> = {
  default: '出现未知错误',
  400: '错误请求',
  401: '未授权，请重新登录',
  403: '拒绝访问',
  404: '请求接口不存在',
  405: '请求方法未允许',
  408: '请求超时',
  500: '服务器端出错',
  501: '网络未实现',
  502: '网络错误',
  503: '服务不可用',
  504: '网络超时',
  505: 'http版本不支持该请求'
};

export enum RESULT_CODE_KEY {
  DEFAULT = 'default',
  REQUEST_ERROR = 400,
  NOT_PERMISSION = 401,
  REQUEST_REJECTED = 403,
  REQUEST_NOT_FOUND = 404,
  REQUEST_METHODS_NOT_ALLOW = 405,
  REQUEST_TIMEOUT = 408,
  SERVER_ERROR = 500,
  // implemented
  NETWORK_NOT_IMPLEMENTED = 501,
  NETWORK_ERROR = 502,
  // SERVICE_Unavailable
  SERVICE_UNAVAILABLE = 503,
  NETWORK_TIMEOUT = 504,
  HTTP_VERSION_NOT_SUPPORTED = 505
}

/**
 * 获取响应报错信息
 * @param {*} code 网关状态吗
 * @param {*} other 自定义响应数据
 * @returns 返回的错误报错信息
 */
function getErroMessage(code: RESULT_CODE_KEY, other: string) {
  return RESULT_CODE[code] || other || RESULT_CODE['default'];
}

export { getErroMessage };
