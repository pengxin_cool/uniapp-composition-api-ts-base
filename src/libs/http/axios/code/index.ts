// 网关层状态码报错处理
import * as NetCode from './net/NetCode';
// 后端自定义业务状态码处理
import * as BusinessCode from './business/BusinessCode';

export { NetCode, BusinessCode };
