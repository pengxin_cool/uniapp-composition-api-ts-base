import { HTTP_CONFIG } from '@/config/index';

/**
 * http请求相关配置
 */
export const HTTP_REQUEST_CONFIG = {
  UPLOAD_HOST: HTTP_CONFIG.UPLOAD_HOST,
  API_HOST: HTTP_CONFIG.API_HOST
};
