import { AxiosRequestConfig, AxiosRequestHeaders, Method, AxiosResponse } from 'axios';
import { ResponseDataType } from '@/libs/http/axios/types/index';

interface RequestLoggerOptions {
  url: string | undefined;
  headers: AxiosRequestHeaders | undefined;
  method: Method | undefined;
  params: Record<string, any> | undefined;
  data: Record<string, any> | undefined;
  response: AxiosResponse<ResponseDataType> | undefined;
  request: AxiosRequestConfig;
}

// 配置是否关闭请求打印日志
export const defaultConfig = {
  debugState: false
};

/**
 * 请求内容日志
 * @param param0 响应数据
 * @returns 无返回
 */
export function requestLogger({ url, headers, method, params, request, data, response }: RequestLoggerOptions) {
  const debugState = Number(process.env.VUE_APP_BASE_API_DEBUG);
  // 配置下如果设置关闭则直接不执行后面的
  if (!debugState) return;
  let currentPage = '在非页面请求';
  if (getCurrentPages && typeof getCurrentPages === 'function') {
    const pages = getCurrentPages();
    currentPage = (pages[pages.length - 1] && pages[pages.length - 1].route) || '非页面触发';
  }
  const log = {
    currentPage,
    url,
    method,
    headers,
    params,
    data,
    request
  };
  console.log('请求的接口：' + url);
  console.log('请求参数与数据为：');
  console.log(log);
  console.log('返回参数为：');
  console.log(response);
}
