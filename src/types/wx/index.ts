/**
 * 微信小程序启动入口的生命周期类型
 */
export type AppType = Partial<WechatMiniprogram.App.Option> & {
  mpType?: string;
};

/**
 * 微信小程序页面的生命周期类型
 */
export type PageLifeTimeType = Partial<WechatMiniprogram.Page.ILifetime>;

export type ComponentLifeTimeType = Partial<WechatMiniprogram.Component.Lifetimes>;
