/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

// 声明组件实例属性或者方法 ( 类似 this.$route )
declare module 'vue/types/vue' {
  interface Vue {
    $example: (msg: string) => void;
  }
}

/**
 * new Vue自定义属性或者方法类似 new Vue({router: router})
 */
declare module 'vue/types/options' {
  import Vue from 'vue';

  interface ComponentOptions<V extends Vue> {
    i18n: any;
  }
}
